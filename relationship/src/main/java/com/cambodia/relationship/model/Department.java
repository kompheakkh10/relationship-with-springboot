package com.cambodia.relationship.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "department")
@Data
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String department;

    @OneToMany(mappedBy = "department")
    private List<Employee> employees;
}
