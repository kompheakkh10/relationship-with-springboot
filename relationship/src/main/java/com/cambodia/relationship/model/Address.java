package com.cambodia.relationship.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer houseNumber;
    private String streetName;
    private int zipCode;

    @OneToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;
}
