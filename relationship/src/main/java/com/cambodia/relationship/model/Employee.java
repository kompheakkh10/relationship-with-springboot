package com.cambodia.relationship.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "employee")
@Data
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstname;
    private String lastname;
    private String role;

    @OneToOne
    @JoinColumn(name = "house_number")
    private Address address;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;
}
